﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Octopus.Library.Utils;
using PurchaseOrderApp.App_Start;

namespace PurchaseOrderApp.Views
{
    public partial class Supplier : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                InitDefaultValue();
                LoadData();                
            }
        }

        private void InitDefaultValue()
        {
            TxtRequester.Text = "PT. Kalbe Farma Tbk.";

            //set default time
            if (string.IsNullOrEmpty(TxtOrderDate.Text))
                TxtOrderDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        private void LoadData()
        {
            DbPurchaseOrder.LoadParameter();
            string query = @"SELECT * FROM tbl_supplier";
            DataTable dt = DbPurchaseOrder.getRecords(query);
            GV_Supplier.DataSource = dt;
            GV_Supplier.DataBind();
        }

        #region table supplier
        protected void GV_Supplier_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "EditRow" || e.CommandName == "DeleteRow")
                {
                    //get id
                    string key = e.CommandArgument.ToString();

                    if (e.CommandName == "EditRow")
                    {
                        //display data
                        ViewRecord(key);
                    }
                    else if (e.CommandName == "DeleteRow")
                    {
                        //delete data
                        DeleteRecord(key);
                    }
                }
            }
            catch { }
        }

        protected void GV_Supplier_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //link button delete
                    foreach (LinkButton button in e.Row.Cells[1].Controls.OfType<LinkButton>())
                    {
                        if (button.CommandName == "DeleteRow")
                        {
                            string key = e.Row.Cells[2].Text;

                            button.Attributes["onclick"] = "if(!confirm('Do you want to delete id " + key + "?')){ return false; };";
                        }
                    }
                }
            }
            catch { }
        }

        protected void GV_Supplier_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //bind gridview
                GV_Supplier.PageIndex = e.NewPageIndex;
                LoadData();
            }
            catch { }
        }
        protected void ViewRecord(string id)
        {
            try
            {
                string query = $"SELECT * FROM tbl_supplier WHERE id = '{id}'";
                DataRow dr = DbPurchaseOrder.getRow(query);
                TxtSupplier.Text = dr["name"].ToString();

                txtPICName.Text = dr["name"].ToString();
                txtEntityName.Text = dr["entity"].ToString();
                txtAddress.Text = dr["address"].ToString();
                txtPhoneNumber.Text = dr["phone"].ToString();
                
                ViewState["IsNewRecord"] = "false";
                ViewState["supplier_id"] = dr["id"].ToString();
                ShowModal();
                //ScriptManager.RegisterStartupScript(this, GetType(), "script", "$('#btnMove').click();", true);
            }
            catch { }
        }

        protected void DeleteRecord(string id)
        {
            try
            {
                string query = $"DELETE FROM tbl_supplier WHERE id = '{id}'";
                DbPurchaseOrder.runCommand(query);

                LoadData();
            }
            catch { }
        }
        #endregion

        #region Add / Update record
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            txtPICName.Text = string.Empty;
            txtEntityName.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtPhoneNumber.Text = string.Empty;

            ViewState["IsNewRecord"] = "true";
            ShowModal();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["IsNewRecord"].ToString() == "true")
                    AddRecord();
                else
                    UpdateRecord();
                
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

            LoadData();
            CloseModal();
        }

        protected void AddRecord()
        {
            try
            {
                string query = $@"INSERT INTO tbl_supplier 
                                VALUES('{txtPICName.Text}',
                                    '{txtEntityName.Text}',
                                    '{txtAddress.Text}',
                                    '{txtPhoneNumber.Text}')";
                DbPurchaseOrder.runCommand(query);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Insert supplier data success');", true);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        protected void UpdateRecord()
        {
            try
            {
                string id = string.Empty;
                if (string.IsNullOrEmpty(ViewState["supplier_id"].ToString()))
                    throw new ArgumentException();
                else
                    id = ViewState["supplier_id"].ToString();

                string query = $@"UPDATE tbl_supplier SET 
                                name = '{txtPICName.Text}',
                                entity = '{txtEntityName.Text}',
                                address = '{txtAddress.Text}',
                                phone = '{txtPhoneNumber.Text}'
                                WHERE id = '{id}'";
                DbPurchaseOrder.runCommand(query);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Update supplier data success');", true);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        protected void ShowModal()
        {
            ScriptManager.RegisterStartupScript(
                (Page)this, base.GetType(),
                "popup",
                "$('#btnPopup').click();",
                true
                );
        }

        protected void CloseModal()
        {
            ScriptManager.RegisterStartupScript(
                (Page)this, base.GetType(),
                "close",
                "$('#btnClose').click();",
                true
                );
        }

        #endregion        
    }
}