﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Demo.aspx.cs" Inherits="PurchaseOrderApp.Views.Demo" MasterPageFile="~/Site.Master"%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="Head" runat="server">
<%--Html Table td alignment--%>

     <style type="text/css">

       .td-align-Right
         {   
         text-align:center;   
        }
        .td-align-left
        {
        text-align: match-parent;
             }
     </style>

    <%--Html Table td alignment End --%>
    <%--Gridview Css--%>

    <style type="text/css">
    SGrid {
    width:auto;
    background-color: #fff;
    margin: 5px 0 10px 0;
    border: solid 1px #ff006e;
    border-collapse:collapse;
}

.SGrid th {
    padding: 4px 2px;
    color: #fff;
    background:#f17c7c;
    border-left: solid 1px #ff006e;
    font-size: 0.9em;
    text-align:center !important;
}
.SGrid td {
    padding: 2px;
    border: solid 1px #ff006e;
    color: #717171;
    text-align:center !important;
}
.SGrid .alt { background: #cec4c4 ; }
.SGrid .pgr { background: #424242 ; }
.SGrid .pgr table { margin: 5px 0; }
.SGrid .pgr td {
    border-width: 0;
    padding: 0 6px;
    border-left: solid 1px #f17c7c;
    font-weight: bold;
    color: #f17c7c;
    line-height: 12px;
    background:#fff;
     text-align:center !important;
 } 
.SGrid .pgr a { color:#424242; text-decoration: none; }
.SGrid .pgr a:hover { color:#ff006e; text-decoration: none; }

    </style>
     <%--Gridview Css End --%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <div>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div align="center">
                <br />
                <br />

                <div class="panel panel-default" style="width: 510px; padding: 5px; margin: 5px">
                    <div id="dvTab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li><a href="#Regtab" aria-controls="Regtab" role="tab" data-toggle="tab">Register</a></li>
                            <li><a href="#Viewtab" aria-controls="Viewtab" role="tab" data-toggle="tab">View</a></li>                                                              
                        </ul>

                        <div class="tab-content" style="padding-top: 10px">
                            <div role="tabpanel" class="tab-pane active" id="Regtab">
                                <div class="table-responsive">
                                    <table style="width:auto" class="table table-striped">                               
                                        <tr>
                                            <td class="td-align-Right"> 
                                                <asp:Label ID="Label1"  CssClass="btn btn-labeled btn-default" runat="server" Text="Name"></asp:Label>
                                            </td>
                                            <td class="td-align-left">
                                                <asp:TextBox ID="txtName" CssClass="form-control" runat="server" ValidationGroup="reg"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" ErrorMessage="*" ForeColor="#FF0066" ValidationGroup="reg"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-align-Right">
                                                <asp:Label ID="Label2" runat="server" CssClass="btn btn-labeled btn-default" Text="Mobile Number"></asp:Label>
                                            </td>
                                            <td class="td-align-left">
                                                <asp:TextBox ID="txtMobile" CssClass="form-control" runat="server" ValidationGroup="reg"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtMobile" ErrorMessage="*" ForeColor="#FF0066" ValidationGroup="reg"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMobile" ErrorMessage="Invalid Number" ForeColor="#CC3300" ValidationExpression="\d{10}" ValidationGroup="reg"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-align-Right">
                                                <asp:Label ID="Label3" runat="server" CssClass="btn btn-labeled btn-default" Text="Email"></asp:Label>
                                            </td>
                                            <td class="td-align-left">
                                                <asp:TextBox ID="txtEmail" CssClass="form-control"  runat="server" ValidationGroup="reg"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmail" ErrorMessage="*" ForeColor="#FF0066" ValidationGroup="reg"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmail" ErrorMessage="Invalid Email" ForeColor="#CC3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="reg"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="td-align-Right">
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:Button ID="Button1" CssClass="btn-danger btn-sm"  runat="server" Text="Submit" OnClick="Button1_Click" ValidationGroup="reg" /> &nbsp;
                                                <asp:Button ID="Button2" runat="server" CssClass="btn-danger btn-sm" Text="Reset" OnClick="Button2_Click" CausesValidation="False" /> &nbsp;
                                                <asp:Button ID="Button3" CssClass="btn-danger btn-sm"  runat="server" Text="View" OnClick="Button3_Click" CausesValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="Viewtab">
                                <div class="table-responsive">                                                                                  
                                    <asp:GridView ID="GridView1" AlternatingRowStyle-CssClass="alt" PagerStyle-CssClass="pgr" CssClass="SGrid" runat="server" AutoGenerateColumns="False">

                                        <AlternatingRowStyle CssClass="alt" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile Number">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("mobile") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("email") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="pgr" />

                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Panel  End-->

                <asp:HiddenField ID="HiddenTab" runat="server" />
            </div>
        </ContentTemplate>
     </asp:UpdatePanel>
    </div>
         
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <script type="text/javascript">

        function Tabs() {

            var Tab = $("#<%=HiddenTab.ClientID%>");
            var tabId = Tab.val() != "" ? Tab.val() : "Regtab";
            $('#dvTab a[href="#' + tabId + '"]').tab('show');
            $("#dvTab a").click(function () {
                Tab.val($(this).attr("href").substring(1));
                //replace("#", ""));
                //substring(1));
            });
        }

        function pageLoad() {
            Tabs();
        }
    </script>
    
</asp:Content>
