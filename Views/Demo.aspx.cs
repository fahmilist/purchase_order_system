﻿using PurchaseOrderApp.App_Start;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PurchaseOrderApp.Views
{
    public partial class Demo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HiddenTab.Value = "Regtab";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                string query = $@"insert into tbl_supplier values('{txtName.Text}','{txtName.Text}','{txtEmail.Text}','{txtMobile.Text}')";
                DbPurchaseOrder.runCommand(query);
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "popup", "alert('Register Sucessfully')", true);
                GridView1.DataSource = null;
                GridView1.DataBind();
                HiddenTab.Value = "Regtab";
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Error", "alert('" + "ERROR : " + ex.Message.ToString() + "')", true);
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            txtName.Text = string.Empty;
            txtMobile.Text = "";
            txtEmail.Text = "";
            txtName.Focus();
            HiddenTab.Value = "Regtab";
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            GridView();
            HiddenTab.Value = "Viewtab";
        }

        protected void GridView()
        {
            try
            {
                string query = $"select * from tbl_supplier";
                DataTable dt = DbPurchaseOrder.getRecords(query);
                
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    int columncount = GridView1.Rows[0].Cells.Count;
                    GridView1.Rows[0].Cells.Clear();
                    // GridView2.FooterRow.Cells.Clear();
                    GridView1.Rows[0].Cells.Add(new TableCell());
                    GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridView1.Rows[0].Cells[0].Text = "No Records Found";
                }
                HiddenTab.Value = "Viewtab";
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Error", "alert('" + "ERROR : " + ex.Message.ToString() + "')", true);
            }
        }
    }
}