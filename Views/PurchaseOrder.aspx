﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseOrder.aspx.cs" Inherits="PurchaseOrderApp.Views.Home" MasterPageFile="~/Site.Master"%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" style="text-align: left;">
                    <li class="active"><a href="#first" data-toggle="tab" id="first_tab">List Purchase Order</a></li>
                    <li><a href="#second" data-toggle="tab" id="second_tab">View Summary Purchase Order</a></li>
                </ul>

                <div class="tab-content">
                    <div id="first" class="tab-pane fade in active">
                        <asp:GridView ID="GV_PurchaseOrder" runat="server"
                            AllowPaging="true"
                            AllowSorting="false"
                            OnRowDataBound="GV_PurchaseOrder_RowDataBound"
                            OnRowCommand="GV_PurchaseOrder_RowCommand"
                            OnPageIndexChanging="GV_PurchaseOrder_PageIndexChanging"
                            PagerStyle-CssClass="arn-pagination"
                            CssClass="table box table-hover table-striped table-bordered">
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="5px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnViewPO" runat="server"
                                            CommandArgument='<%# Eval("Order No") %>'
                                            CommandName="ViewRow"
                                            CssClass="fa fa-file-pdf-o"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="5px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" runat="server"
                                            CommandArgument='<%# Eval("Order No") %>'
                                            CommandName="EditRow"
                                            CssClass="fa fa-edit"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="5px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDelete" runat="server"
                                            CommandArgument='<%# Eval("Order No") %>'
                                            CommandName="DeleteRow"
                                            CssClass="fa fa-remove"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>                                
                            </Columns>
                        </asp:GridView>                        
                        <asp:LinkButton ID="btnAdd" CssClass="btn btn-primary btn-sm" runat="server" OnClick="btnAdd_Click">ADD</asp:LinkButton>
                        <%-- hide button edit PurchaseOrder--%>
                        <a class="btn btn-primary btn-md" onclick="$('#second_tab').trigger('click')" id="btnMove" style="display: none">Edit</a>
                        <%-- hide button add PurchaseOrder--%>
                        <button type="button" id="btnPopup" data-toggle="modal" data-target="#myModal"
                            class="btn btn-primary btn-sm" style="display: none">
                        </button>
                    </div>
      
                    <div id="second" class="tab-pane fade in">
                        <div class="box-body">
                            <!-- Content Header (Page header) -->
                            <section class="content-header">
                                <h1>
                                PURCHASE ORDER
                                <small><asp:Label ID="lblOrderNo" runat="server" Text="lblOrderNo"></asp:Label></small>
                                </h1>
                            </section>

                            <!-- Main content -->
                            <section class="invoice">
                                <!-- title row -->
                                <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    <i class="fa fa-globe"></i> <asp:Label ID="lblEntityName" runat="server" Text="lblEntityName"></asp:Label>
                                    <small class="pull-right">
                                        Date : <asp:Label ID="lblOrderDate" runat="server" Text="lblOrderDate"></asp:Label>
                                    </small>
                                    </h2>
                                </div>
                                <!-- /.col -->
                                </div>
                                <!-- info row -->
                                <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    VENDOR
                                    <address>
                                    <strong><asp:Label ID="lblVendorContactName" runat="server" Text="lblVendorContactName"></asp:Label></strong><br />
                                    <asp:Label ID="lblVendorCompanyName" runat="server" Text="lblVendorCompanyName"></asp:Label><br>
                                    <asp:Label ID="lblVendorAddress" runat="server" Text="lblVendorAddress"></asp:Label><br>
                                    Phone: <asp:Label ID="lblVendorPhone" runat="server" Text="lblVendorPhone"></asp:Label><br>
                                    </address>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    SHIP TO
                                    <address>
                                    <strong>
                                        <asp:Label ID="lblWarehouseContactName" runat="server" Text="lblWarehouseContactName"></asp:Label> ; 
                                        <asp:Label ID="lblWarehouseDepartmentName" runat="server" Text="lblWarehouseDepartmentName"></asp:Label>
                                    </strong><br>
                                    <asp:Label ID="lblWarehouseCompanyName" runat="server" Text="lblWarehouseCompanyName"></asp:Label><br>
                                    <asp:Label ID="lblWarehouseAddress" runat="server" Text="lblWarehouseAddress"></asp:Label><br>
                                    Phone: <asp:Label ID="lblWarehousePhone" runat="server" Text="lblWarehousePhone"></asp:Label><br>
                                    </address>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <b>PO REFERENCE NO #<asp:Label ID="lblOrderNo2" runat="server" Text="lblOrderNo2"></asp:Label></b><br>
                                </div>
                                <!-- /.col -->
                                </div>
                                <!-- /.row -->

                                <!-- Table row -->
                                <asp:GridView ID="GV_PDF_SHIPPING" runat="server"
                                    CssClass="table box table-hover table-striped table-bordered"
                                    AllowSorting="false">
                                </asp:GridView>
                                <hr />
                                <asp:GridView ID="GV_PDF_PRODUCT" runat="server"
                                    CssClass="table box table-hover table-striped table-bordered"
                                    AllowSorting="false">
                                </asp:GridView>
                                <!-- /.row -->

                                <div class="row">
                                <!-- accepted payments column -->
                                <div class="col-xs-6">
                                    <p class="lead">Payment Methods:</p>
                                    <img src="/admin-lte/img/credit/visa.png" alt="Visa">
                                    <img src="/admin-lte/img/credit/mastercard.png" alt="Mastercard">
                                    <img src="/admin-lte/img/credit/american-express.png" alt="American Express">
                                    <img src="/admin-lte/img/credit/paypal2.png" alt="Paypal">

                                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                        <asp:Label ID="lblRemarks" runat="server" Text="lblRemarks"></asp:Label>
                                    </p>
                                </div>
                                <!-- /.col -->
                                <div class="col-xs-6">
                                    <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th style="width:50%">Subtotal:</th>
                                            <td>
                                                <asp:Label ID="lblSubTotal" runat="server" Text="lblSubTotal"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:50%">Discount:</th>
                                            <td>
                                                <asp:Label ID="lblDiscount" runat="server" Text="lblDiscount"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:50%">Subtotal Less Discount:</th>
                                            <td>
                                                <asp:Label ID="lblSubTotalDiscount" runat="server" Text="lblSubTotalDiscount"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Tax (<asp:Label ID="lblTaxRate" runat="server" Text="lblTaxRate"></asp:Label>%)</th>
                                            <td>IDR <asp:Label ID="lblTotalTax" runat="server" Text="lblTotalTax"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <th>Shipping:</th>
                                            <td>IDR <asp:Label ID="lblShippingCost" runat="server" Text="lblShippingCost"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <th>Total:</th>
                                            <td>IDR <asp:Label ID="lblTotal" runat="server" Text="lblTotal"></asp:Label></td>
                                        </tr>
                                    </table>
                                    </div>
                                </div>
                                <!-- /.col -->
                                </div>
                                <!-- /.row -->

                                <!-- this row will not appear when printing -->
                                <div class="row no-print">
                                    <div class="col-xs-12">
                                        <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;" onclick="window.print()">
                                        <i class="fa fa-download"></i> Generate PDF
                                        </button>
                                    </div>
                                </div>
                            </section>
                            <!-- /.content -->
                            <div class="clearfix"></div>
                    
                        </div>
                        <%--<a class="btn btn-primary btn-md" onclick="$('#first_tab').trigger('click')">Add</a>--%>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div class="modal" id="myModal">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div class="modal-dialog-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Input Purchase Order</h4>
                        </div>
                        <div class="modal-body">
                            <div class="nav-tabs-custom">
                                <%--TAB MENU--%>
                                <div id="Tabs" role="tabpanel">
                                    <ul class="nav nav-tabs bg-gray-light">
                                        <li><a href="#tabHeader" data-toggle="tab" id="clickTabHeader">Header</a></li>
                                        <li><a href="#tabVendor" data-toggle="tab" id="clickTabVendor">Vendor</a></li>
                                        <li><a href="#tabCompany" data-toggle="tab" id="clickTabCompany">Company</a></li>
                                        <li><a href="#tabShipping" data-toggle="tab" id="clickTabShipping">Shipping</a></li>
                                        <li><a href="#tabProduct" data-toggle="tab" id="clickTabProduct">Product</a></li>
                                    </ul>

                                    <asp:HiddenField ID="TabName" runat="server" value="clickTabHeader"/>
                                </div>

                                <%--TAB CONTENT--%>
                                <div class="tab-content">
                                    <%--TAB HEADER--%>
                                    <div class="tab-pane active" id="tabHeader">
                                        <div class="form-horizontal">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="txtEntityName" class="col-sm-3 control-label">Entity name</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtEntityName" runat="server" CssClass="form-control" MaxLength="100" required="true"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtEntityAddress" class="col-sm-3 control-label">Entity address</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtEntityAddress" runat="server" CssClass="form-control" MaxLength="100" required="true"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtEntityPhone" class="col-sm-3 control-label">Entity phone</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtEntityPhone" runat="server" CssClass="form-control" MaxLength="16" required="true" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))"></asp:TextBox>                                            
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtOrderNo" class="col-sm-3 control-label">Order no</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtOrderNo" runat="server" CssClass="form-control" MaxLength="100" required="true"></asp:TextBox>                                            
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtOrderDate" class="col-sm-3 control-label">Order date</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtOrderDate" runat="server" placeholder="yyyy-mm-dd" CssClass="form-control" MaxLength="10" required="true"></asp:TextBox>                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <%--TAB VENDOR--%>
                                    <div class="tab-pane" id="tabVendor">
                                        <div class="form-horizontal">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="txtVendorCompanyName" class="col-sm-3 control-label">Company name</label>

                                                    <div class="col-sm-9">
                                                        <asp:DropDownList ID="ddlVendorCompanyName" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlVendorCompanyName_SelectedIndexChanged" AutoPostBack="true">
                                                        </asp:DropDownList>                                                        
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtVendorContactName" class="col-sm-3 control-label">Contact name</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtVendorContactName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvVendorContactName" runat="server" SetFocusOnError="true" ControlToValidate="txtVendorContactName" ValidationGroup="popUpGroup" ErrorMessage="*Required to fill contact name" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>                                                

                                                <div class="form-group">
                                                    <label for="txtVendorAddress" class="col-sm-3 control-label">Address</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtVendorAddress" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvVendorAddress" runat="server" SetFocusOnError="true" ControlToValidate="txtVendorAddress" ValidationGroup="popUpGroup" ErrorMessage="*Required to fill address" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtVendorPhone" class="col-sm-3 control-label">Phone</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtVendorPhone" runat="server" CssClass="form-control" MaxLength="16" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvVendorPhone" runat="server" SetFocusOnError="true" ControlToValidate="txtVendorPhone" ValidationGroup="popUpGroup" ErrorMessage="*Required to fill phone" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <%--TAB COMPANY--%>
                                    <div class="tab-pane" id="tabCompany">
                                        <div class="form-horizontal">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="txtWarehouseCompanyName" class="col-sm-3 control-label">Company name</label>

                                                    <div class="col-sm-9">
                                                        <asp:DropDownList ID="ddlWarehouseCompanyName" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlWarehouseCompanyName_SelectedIndexChanged" AutoPostBack="true">
                                                        </asp:DropDownList>                                                        
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtWarehouseContactName" class="col-sm-3 control-label">Contact name</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtWarehouseContactName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvWarehouseContactName" runat="server" SetFocusOnError="true" ControlToValidate="txtWarehouseContactName" ValidationGroup="popUpGroup" ErrorMessage="*Required to fill contact name" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="txtWarehouseDepartmentName" class="col-sm-3 control-label">Department name</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtWarehouseDepartmentName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvWarehouseDepartmentName" runat="server" SetFocusOnError="true" ControlToValidate="txtWarehouseDepartmentName" ValidationGroup="popUpGroup" ErrorMessage="*Required to fill department name" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtWarehouseAddress" class="col-sm-3 control-label">Address</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtWarehouseAddress" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvWarehouseAddress" runat="server" SetFocusOnError="true" ControlToValidate="txtWarehouseAddress" ValidationGroup="popUpGroup" ErrorMessage="*Required to fill address" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtWarehousePhone" class="col-sm-3 control-label">Phone</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtWarehousePhone" runat="server" CssClass="form-control" MaxLength="16" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))"></asp:TextBox>                                            
                                                        <asp:RequiredFieldValidator ID="rfvWarehousePhone" runat="server" SetFocusOnError="true" ControlToValidate="txtWarehousePhone" ValidationGroup="popUpGroup" ErrorMessage="*Required to fill phone" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <%--TAB SHIPPING--%>
                                    <div class="tab-pane" id="tabShipping">
                                        <div class="form-horizontal">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="txtShippingName" class="col-sm-3 control-label">Shipping name</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtShippingName" runat="server" CssClass="form-control" MaxLength="25"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvShippingName" runat="server" SetFocusOnError="true" ControlToValidate="txtShippingName" ValidationGroup="shippingGroup" ErrorMessage="*Required to fill shipping name" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtShippingMethod" class="col-sm-3 control-label">Shipping method</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtShippingMethod" runat="server" CssClass="form-control" MaxLength="25"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvShippingMethod" runat="server" SetFocusOnError="true" ControlToValidate="txtShippingMethod" ValidationGroup="shippingGroup" ErrorMessage="*Required to fill shipping method" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="txtShippingTerms" class="col-sm-3 control-label">Shipping terms</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtShippingTerms" runat="server" CssClass="form-control" MaxLength="25"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvShippingTerms" runat="server" SetFocusOnError="true" ControlToValidate="txtShippingTerms" ValidationGroup="shippingGroup" ErrorMessage="*Required to fill shipping terms" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtDeliveryDate" class="col-sm-3 control-label">Delivery date</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtDeliveryDate" runat="server" placeholder="yyyy-mm-dd" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvDeliveryDate" runat="server" SetFocusOnError="true" ControlToValidate="txtDeliveryDate" ValidationGroup="shippingGroup" ErrorMessage="*Required to fill delivery date" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <asp:Button ID="btnShippingSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnShippingSave_Click" ValidationGroup="shippingGroup"/>
                                                <hr />
                                                <asp:GridView ID="GV_Shipping" runat="server"
                                                    CssClass="table box table-hover table-striped table-bordered"
                                                    OnRowDataBound="GV_Shipping_RowDataBound"
                                                    OnRowCommand="GV_Shipping_RowCommand"
                                                    AllowSorting="false">
                                                    <Columns>
                                                    <asp:TemplateField ItemStyle-Width="5px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteShipping" runat="server"
                                                                CommandArgument='<%# Eval("ID") %>'
                                                                CommandName="DeleteRow"
                                                                CssClass="fa fa-remove"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <%--TAB PRODUCT--%>
                                    <div class="tab-pane" id="tabProduct">
                                        <div class="form-horizontal">
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="txtProductCode" class="col-sm-3 control-label">Product code</label>

                                                    <div class="col-sm-9">
                                                        <asp:DropDownList ID="ddlProductCode" runat="server" CssClass="form-control select2" OnSelectedIndexChanged="ddlProductCode_SelectedIndexChanged" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtProductName" class="col-sm-3 control-label">Product name</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtProductName" runat="server" CssClass="form-control" MaxLength="100" ReadOnly="true"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvProductName" runat="server" SetFocusOnError="true" ControlToValidate="txtProductName" ValidationGroup="productGroup" ErrorMessage="*Required to fill product name" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="txtQuantity" class="col-sm-3 control-label">Quantity</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" MaxLength="11" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvQuantity" runat="server" SetFocusOnError="true" ControlToValidate="txtQuantity" ValidationGroup="productGroup" ErrorMessage="*Required to fill quantity" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtUnitPrice" class="col-sm-3 control-label">Unit price</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="form-control" MaxLength="16" ReadOnly="true"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvUnitPrice" runat="server" SetFocusOnError="true" ControlToValidate="txtUnitPrice" ValidationGroup="productGroup" ErrorMessage="*Required to fill unit price" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtProductDeliveryDate" class="col-sm-3 control-label">Delivery date</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtProductDeliveryDate" runat="server" placeholder="yyyy-mm-dd" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvProductDeliveryDate" runat="server" SetFocusOnError="true" ControlToValidate="txtProductDeliveryDate" ValidationGroup="productGroup" ErrorMessage="*Required to fill delivery date" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <asp:Button ID="btnProductSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnProductSave_Click" ValidationGroup="productGroup"/>
                                                <hr />
                                                <asp:GridView ID="GV_Product" runat="server"
                                                    CssClass="table box table-hover table-striped table-bordered"
                                                    OnRowDataBound="GV_Product_RowDataBound"
                                                    OnRowCommand="GV_Product_RowCommand"
                                                    AllowSorting="false">
                                                    <Columns>
                                                    <asp:TemplateField ItemStyle-Width="5px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteProduct" runat="server"
                                                                CommandArgument='<%# Eval("ID") %>'
                                                                CommandName="DeleteRow"
                                                                CssClass="fa fa-remove"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                </asp:GridView>
                                                <hr />
                                                <div class="form-group">
                                                    <label for="txtSubTotal" class="col-sm-3 control-label">Sub total</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtSubTotal" runat="server" CssClass="form-control" MaxLength="16" ReadOnly="true"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvSubTotal" runat="server" SetFocusOnError="true" ControlToValidate="txtSubTotal" ValidationGroup="popUpGroup" ErrorMessage="*Sub total cannot be zero" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtDiscount" class="col-sm-3 control-label">Discount (%)</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtDiscount" runat="server" CssClass="form-control" MaxLength="3" AutoPostBack="true" OnTextChanged="txtDiscount_TextChanged"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvtxtDiscount" runat="server" SetFocusOnError="true" ControlToValidate="txtDiscount" ValidationGroup="popUpGroup" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>                                                    
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtSubTotalDiscount" class="col-sm-3 control-label">Sub total less discount</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtSubTotalDiscount" runat="server" CssClass="form-control" MaxLength="16" ReadOnly="true"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvSubTotalDiscount" runat="server" SetFocusOnError="true" ControlToValidate="txtSubTotalDiscount" ValidationGroup="popUpGroup" ErrorMessage="*Sub total cannot be zero" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtTaxRate" class="col-sm-3 control-label">Tax rate</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtTaxRate" runat="server" CssClass="form-control" MaxLength="3" AutoPostBack="true" OnTextChanged="txtTaxRate_TextChanged"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvTaxRate" runat="server" SetFocusOnError="true" ControlToValidate="txtTaxRate" ValidationGroup="popUpGroup" ErrorMessage="*Required" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtTotalTax" class="col-sm-3 control-label">Total tax</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtTotalTax" runat="server" CssClass="form-control" MaxLength="16" ReadOnly="true"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvTotalTax" runat="server" SetFocusOnError="true" ControlToValidate="txtTotalTax" ValidationGroup="popUpGroup" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtShippingCost" class="col-sm-3 control-label">Shipping cost</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtShippingCost" runat="server" CssClass="form-control" MaxLength="16" ReadOnly="true"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvShippingCost" runat="server" SetFocusOnError="true" ControlToValidate="txtShippingCost" ValidationGroup="popUpGroup" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtTotal" class="col-sm-3 control-label">Total</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtTotal" runat="server" CssClass="form-control" MaxLength="16" ReadOnly="true"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvTotal" runat="server" SetFocusOnError="true" ControlToValidate="txtTotal" ValidationGroup="popUpGroup" ErrorMessage="*Total cannot be zero" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="txtRemarks" class="col-sm-3 control-label">Remarks</label>

                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            
                        </div><%-- /.modal-body --%>
                        <div class="modal-footer">
                            <button id="btnClose" type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSave_Click" ValidationGroup="popUpGroup" />
                        </div>
                    </div>
                    <%-- /.modal-content --%>
                </div>
                <%-- /.modal-dialog --%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script>
        $(document).ready(function () {

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $(<%=txtOrderDate.ClientID%>).datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true
                });

                $(<%=txtDeliveryDate.ClientID%>).datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true
                });

                $(<%=txtProductDeliveryDate.ClientID%>).datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true
                });                
                
                //$('#Tabs a[href="' + tab + '"]').tab('show');
                var tab = document.getElementById('<%= TabName.ClientID%>').value;
                var x = $('#' + tab);
                x.tab('show');

            }
        });
    </script>
</asp:Content>
