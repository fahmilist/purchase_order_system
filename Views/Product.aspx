﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="PurchaseOrderApp.Views.Product" MasterPageFile="~/Site.Master"%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" style="text-align: left;">
                    <li class="active"><a href="#first" data-toggle="tab" id="first_tab">List Product</a></li>
                    <%--<li><a href="#second" data-toggle="tab" id="second_tab">Add Product</a></li>--%>
                </ul>
    
                <div class="tab-content">
                    <div id="first" class="tab-pane fade in active">
                        <asp:GridView ID="GV_Product" runat="server"
                            AllowPaging="true"
                            AllowSorting="false"
                            OnRowDataBound="GV_Product_RowDataBound"
                            OnRowCommand="GV_Product_RowCommand"
                            OnPageIndexChanging="GV_Product_PageIndexChanging"
                            PagerStyle-CssClass="arn-pagination"
                            CssClass="table box table-hover table-striped table-bordered">
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="5px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" runat="server"
                                            CommandArgument='<%# Eval("product_code") %>'
                                            CommandName="EditRow"
                                            CssClass="fa fa-edit"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="5px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDelete" runat="server"
                                            CommandArgument='<%# Eval("product_code") %>'
                                            CommandName="DeleteRow"
                                            CssClass="fa fa-remove"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>                        
                        <asp:LinkButton ID="btnAdd" CssClass="btn btn-primary btn-sm" runat="server" OnClick="btnAdd_Click">ADD</asp:LinkButton>
                        <%-- hide button edit Product--%>
                        <a class="btn btn-primary btn-md" onclick="$('#second_tab').trigger('click')" id="btnMove" style="display: none">Edit</a>
                        <%-- hide button add Product--%>
                        <button type="button" id="btnPopup" data-toggle="modal" data-target="#myModal"
                            class="btn btn-primary btn-sm" style="display: none">
                        </button>
                    </div>
      
                    <div id="second" class="tab-pane fade in">
                        <div class="box-body">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label class="control-label">Requester</label>
                                    </div>                                
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="TxtRequester" runat="server" placeholder="Requester" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                    </div>                       
                                </div>                   
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label class="control-label">Order Date</label>
                                    </div>                                
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="TxtOrderDate" runat="server" placeholder="yyyy-mm-dd" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label class="control-label">Product name</label>
                                    </div>                                
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="TxtProduct" runat="server" placeholder="Requester" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                    
                        </div>
                        <a class="btn btn-primary btn-md" onclick="$('#first_tab').trigger('click')">Add</a>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    
    <div class="modal" id="myModal">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Input Product</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="txtProductCode" class="col-sm-3 control-label">Product code</label>

                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtProductCode" runat="server" CssClass="form-control" MaxLength="50" required="true"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="txtProductName" class="col-sm-3 control-label">Product name</label>

                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtProductName" runat="server" CssClass="form-control" MaxLength="50" required="true"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="txtProductPrice" class="col-sm-3 control-label">Product price</label>

                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtProductPrice" runat="server" CssClass="form-control price" MaxLength="16" required="true" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))"></asp:TextBox>
                                            <small>
                                                <asp:RangeValidator ID="RangeValidator" ValidationGroup="priceGroup" runat="server" ControlToValidate="txtProductPrice" Text="Please enter number only!" ForeColor="Red" SetFocusOnError="True" Type="Double"></asp:RangeValidator>
                                                <asp:RequiredFieldValidator runat="server" ValidationGroup="priceGroup" ControlToValidate="txtProductPrice" Text="*Required!" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="btnClose" type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" ValidationGroup="priceGroup" OnClick="btnSave_Click" />
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script>
        $(function () {
            //Date picker
            $(<%=TxtOrderDate.ClientID%>).datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
        })
    </script>
</asp:Content>