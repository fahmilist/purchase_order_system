﻿using PurchaseOrderApp.App_Start;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PurchaseOrderApp.Views
{
    public partial class Home : System.Web.UI.Page
    {
        DataTable dt = new DataTable();
        DataTable dt2 = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
                InitDropdown();

                if (ViewState["ShippingRecord"] == null)
                {
                    dt.Columns.Add("ID");
                    dt.Columns.Add("Order No");
                    dt.Columns.Add("Shipping Name");
                    dt.Columns.Add("Shipping Method");
                    dt.Columns.Add("Shipping Terms");
                    dt.Columns.Add("Delivery Date");
                    ViewState["ShippingRecord"] = dt;
                }

                if (ViewState["ProductRecord"] == null)
                {
                    dt2.Columns.Add("ID");
                    dt2.Columns.Add("Order No");
                    dt2.Columns.Add("Product Code");
                    dt2.Columns.Add("Product Name");
                    dt2.Columns.Add("Quantity");
                    dt2.Columns.Add("Unit Price");
                    dt2.Columns.Add("Delivery Date");
                    ViewState["ProductRecord"] = dt2;
                }
            }
        }

        protected void LoadData()
        {
            DbPurchaseOrder.LoadParameter();
            string query = @"SELECT
                            order_number AS 'Order No',
                            CONVERT(VARCHAR,order_date,23) AS 'Order Date',
                            sp.name AS 'Vendor',
                            wh.name AS 'Company Warehouse',
                            total AS 'Total'

                            FROM tbl_purchase_order po JOIN tbl_supplier sp
                            ON po.supplier_id = sp.id JOIN tbl_warehouse wh
                            ON po.warehouse_id = wh.id
                            ORDER BY order_number DESC";
            DataTable dt = DbPurchaseOrder.getRecords(query);
            GV_PurchaseOrder.DataSource = dt;
            GV_PurchaseOrder.DataBind();
        }
        
        protected void InitDropdown()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("SUPPLIER", "SELECT * FROM tbl_supplier");
            data.Add("WAREHOUSE", "SELECT * FROM tbl_warehouse");
            data.Add("PRODUCT", "SELECT * FROM tbl_product");

            ViewState.Add("DataMisc", data);

            InitSupplier(data["SUPPLIER"]);
            InitWarehouse(data["WAREHOUSE"]);
            InitProduct(data["PRODUCT"]);
        }

        protected void InitSupplier(string data)
        {
            ddlVendorCompanyName.Items.Clear();
            ddlVendorCompanyName.Items.Add("");

            DataTable dt = DbPurchaseOrder.getRecords(data);            

            //add item
            foreach (DataRow dr in dt.Rows)
            {
                //adding dropdown
                ddlVendorCompanyName.Items.Add(new ListItem
                {
                    Value = dr["id"].ToString(),
                    Text = dr["entity"].ToString()
                });
            }
        }

        protected void InitWarehouse(string data)
        {
            ddlWarehouseCompanyName.Items.Clear();
            ddlWarehouseCompanyName.Items.Add("");

            DataTable dt = DbPurchaseOrder.getRecords(data);

            //add item
            foreach (DataRow dr in dt.Rows)
            {
                //adding dropdown
                ddlWarehouseCompanyName.Items.Add(new ListItem
                {
                    Value = dr["id"].ToString(),
                    Text = dr["entity"].ToString()
                });
            }
        }

        protected void InitProduct(string data)
        {
            ddlProductCode.Items.Clear();
            ddlProductCode.Items.Add("");

            DataTable dt = DbPurchaseOrder.getRecords(data);

            //add item
            foreach (DataRow dr in dt.Rows)
            {
                //adding dropdown
                ddlProductCode.Items.Add(new ListItem
                {
                    Value = dr["product_code"].ToString(),
                    Text = dr["product_code"].ToString()
                });
            }
        }

        #region table purchase order
        protected void GV_PurchaseOrder_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "EditRow" || e.CommandName == "DeleteRow" || e.CommandName == "ViewRow")
                {
                    //get id
                    string key = e.CommandArgument.ToString();

                    if (e.CommandName == "EditRow")
                    {
                        //display data
                        ViewRecord(key);
                    }
                    else if (e.CommandName == "DeleteRow")
                    {
                        //delete data
                        DeleteRecord(key);
                    }
                    else if (e.CommandName == "ViewRow")
                    {
                        PdfViewRecord(key);
                        ScriptManager.RegisterStartupScript(this, GetType(), "script", "$('#btnMove').click();", true);
                    }
                }
            }
            catch { }
        }

        protected void GV_PurchaseOrder_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //link button delete
                    foreach (LinkButton button in e.Row.Cells[1].Controls.OfType<LinkButton>())
                    {
                        if (button.CommandName == "DeleteRow")
                        {
                            string key = e.Row.Cells[2].Text;

                            button.Attributes["onclick"] = "if(!confirm('Do you want to delete order no " + key + "?')){ return false; };";
                        }
                    }
                }
            }
            catch { }
        }

        protected void GV_PurchaseOrder_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //bind gridview
                GV_PurchaseOrder.PageIndex = e.NewPageIndex;
                LoadData();
            }
            catch { }
        }
        protected void ViewRecord(string id)
        {
            TabName.Value = "clickTabHeader";
            try
            {
                string query = $@"SELECT
                                order_number,
                                order_date,
                                notes,
                                header_entity_name,
                                header_entity_address,
                                header_entity_phone,
                                sp.id AS vendor_id,
                                sp.entity AS vendor_company_name,
                                sp.name AS vendor_contact_name,
                                sp.address AS vendor_address,
                                sp.phone AS vendor_phone,
                                wh.id AS warehouse_id,
                                wh.entity AS warehouse_entity_name,
                                wh.name AS warehouse_contact_name,
                                wh.title AS warehouse_department_name,
                                wh.address AS warehouse_address,
                                wh.phone AS warehouse_phone

                                FROM tbl_purchase_order po JOIN tbl_supplier sp
                                ON po.supplier_id = sp.id JOIN tbl_warehouse wh
                                ON po.warehouse_id = wh.id
                                WHERE po.order_number = '{id}'";
                DataRow dr = DbPurchaseOrder.getRow(query);

                //tab header
                txtOrderNo.Text = dr["order_number"].ToString();

                //change format date
                DateTime orderDate = Convert.ToDateTime(dr["order_date"].ToString());
                txtOrderDate.Text = orderDate.ToString("yyyy-MM-dd");

                txtEntityName.Text = dr["header_entity_name"].ToString();
                txtEntityAddress.Text = dr["header_entity_address"].ToString();
                txtEntityPhone.Text = dr["header_entity_phone"].ToString();

                //disable edit order no
                txtOrderNo.ReadOnly = true;

                //tab vendor
                ddlVendorCompanyName.SelectedItem.Text = dr["vendor_company_name"].ToString();
                ddlVendorCompanyName.SelectedItem.Value = dr["vendor_id"].ToString();
                txtVendorContactName.Text = dr["vendor_contact_name"].ToString();
                txtVendorAddress.Text = dr["vendor_address"].ToString();
                txtVendorPhone.Text = dr["vendor_phone"].ToString();

                //tab warehouse
                ddlWarehouseCompanyName.SelectedItem.Text = dr["warehouse_entity_name"].ToString();
                ddlWarehouseCompanyName.SelectedItem.Value = dr["warehouse_id"].ToString();
                txtWarehouseContactName.Text = dr["warehouse_contact_name"].ToString();
                txtWarehouseDepartmentName.Text = dr["warehouse_department_name"].ToString();
                txtWarehouseAddress.Text = dr["warehouse_address"].ToString();
                txtWarehousePhone.Text = dr["warehouse_phone"].ToString();

                //tab Shipping
                query = $@"SELECT
                        id AS ID,
                        order_number AS 'Order No',
                        shipping_name AS 'Shipping Name',
                        shipping_method AS 'Shipping Method',
                        shipping_terms AS 'Shipping Terms',
                        CONVERT(VARCHAR,delivery_date,23) AS 'Delivery Date'
                        FROM tbl_shipping_details
                        WHERE order_number = '{dr["order_number"].ToString()}'";
                DataTable dt = DbPurchaseOrder.getRecords(query);
                ViewState["ShippingRecord"] = dt;
                txtDeliveryDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                LoadShippingData();

                //tab Product
                query = $@"SELECT
                        id AS ID,
                        pd.order_number AS 'Order No',
                        pd.product_code AS 'Product Code',
                        p.product_name AS 'Product Name',
                        pd.quantity AS 'Quantity',
                        p.product_price AS 'Unit Price',
                        CONVERT(VARCHAR,pd.delivery_date,23) AS 'Delivery Date'
                        FROM tbl_product_details pd JOIN tbl_product p
                        ON pd.product_code = p.product_code
                        WHERE pd.order_number = '{dr["order_number"].ToString()}'";
                DataTable dt2 = DbPurchaseOrder.getRecords(query);
                ViewState["ProductRecord"] = dt2;
                txtProductDeliveryDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                LoadProductData();

                txtRemarks.Text = dr["notes"].ToString();

                ViewState["IsNewRecord"] = "false";
                ShowModal();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        protected void DeleteRecord(string id)
        {
            try
            {
                string query = $"DELETE FROM tbl_purchase_order WHERE order_number = '{id}'";
                DbPurchaseOrder.runCommand(query);

                LoadData();
            }
            catch { }
        }

        protected void PdfViewRecord(string id) 
        {
            try
            {
                string query = $@"SELECT
                                order_number,
                                order_date,
                                notes,
                                header_entity_name,
                                header_entity_address,
                                header_entity_phone,
                                sp.id AS vendor_id,
                                sp.entity AS vendor_company_name,
                                sp.name AS vendor_contact_name,
                                sp.address AS vendor_address,
                                sp.phone AS vendor_phone,
                                wh.id AS warehouse_id,
                                wh.entity AS warehouse_entity_name,
                                wh.name AS warehouse_contact_name,
                                wh.title AS warehouse_department_name,
                                wh.address AS warehouse_address,
                                wh.phone AS warehouse_phone,

                                subtotal,discount,tax_rate,shipping_cost,total

                                FROM tbl_purchase_order po JOIN tbl_supplier sp
                                ON po.supplier_id = sp.id JOIN tbl_warehouse wh
                                ON po.warehouse_id = wh.id
                                WHERE po.order_number = '{id}'";
                DataRow dr = DbPurchaseOrder.getRow(query);

                //tab header
                lblEntityName.Text = dr["header_entity_name"].ToString();
                lblOrderNo.Text = dr["order_number"].ToString();
                lblOrderNo2.Text = dr["order_number"].ToString();

                //change format date
                DateTime orderDate = Convert.ToDateTime(dr["order_date"].ToString());
                lblOrderDate.Text = orderDate.ToString("yyyy-MM-dd");

                //tab vendor
                lblVendorCompanyName.Text = dr["vendor_company_name"].ToString();
                lblVendorContactName.Text = dr["vendor_contact_name"].ToString();
                lblVendorAddress.Text = dr["vendor_address"].ToString();
                lblVendorPhone.Text = dr["vendor_phone"].ToString();

                //tab warehouse
                lblWarehouseCompanyName.Text = dr["warehouse_entity_name"].ToString();
                lblWarehouseContactName.Text = dr["warehouse_contact_name"].ToString();
                lblWarehouseDepartmentName.Text = dr["warehouse_department_name"].ToString();
                lblWarehouseAddress.Text = dr["warehouse_address"].ToString();
                lblWarehousePhone.Text = dr["warehouse_phone"].ToString();

                //tab Shipping
                query = $@"SELECT
                        id AS ID,
                        order_number AS 'Order No',
                        shipping_name AS 'Shipping Name',
                        shipping_method AS 'Shipping Method',
                        shipping_terms AS 'Shipping Terms',
                        CONVERT(VARCHAR,delivery_date,23) AS 'Delivery Date'
                        FROM tbl_shipping_details
                        WHERE order_number = '{dr["order_number"].ToString()}'";
                DataTable dt = DbPurchaseOrder.getRecords(query);
                ViewState["ShippingRecord"] = dt;
                LoadPDFShippingData();

                //tab Product
                query = $@"SELECT
                        id AS ID,
                        pd.order_number AS 'Order No',
                        pd.product_code AS 'Product Code',
                        p.product_name AS 'Product Name',
                        pd.quantity AS 'Quantity',
                        p.product_price AS 'Unit Price',
                        CONVERT(VARCHAR,pd.delivery_date,23) AS 'Delivery Date'
                        FROM tbl_product_details pd JOIN tbl_product p
                        ON pd.product_code = p.product_code
                        WHERE pd.order_number = '{dr["order_number"].ToString()}'";
                DataTable dt2 = DbPurchaseOrder.getRecords(query);
                ViewState["ProductRecord"] = dt2;
                LoadPDFProductData();

                lblRemarks.Text = dr["notes"].ToString();
                lblSubTotal.Text = dr["subtotal"].ToString();
                lblDiscount.Text = dr["discount"].ToString();
                lblTaxRate.Text = dr["tax_rate"].ToString();
                lblShippingCost.Text = dr["shipping_cost"].ToString();
                lblTotal.Text = dr["total"].ToString();

                int sub_total_discount = 0;
                string sub_total = dr["subtotal"].ToString();

                sub_total_discount += Convert.ToInt32(Convert.ToDouble(sub_total) * ((100 - Convert.ToDouble(dr["discount"].ToString())) / 100));
                lblSubTotalDiscount.Text = sub_total_discount.ToString();

                int total_tax = 0;
                total_tax += Convert.ToInt32(Convert.ToDouble(sub_total) * (Convert.ToDouble(dr["discount"].ToString()) / 100));
                lblTotalTax.Text = total_tax.ToString();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        #endregion

        #region Add / Update record
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            TabName.Value = "clickTabHeader";
            txtOrderNo.Text = GetOrderNo();
            txtOrderDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtEntityName.Text = "PT. Kalbe Farma Tbk.";
            txtEntityAddress.Text = "Jl. Rawa Gatel, Pulogadung.";
            txtEntityPhone.Text = "0214600158";

            ClearFormVendor();
            ClearFormWarehouse();

            txtDeliveryDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

            txtQuantity.Text = "0";
            txtProductDeliveryDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

            txtSubTotal.Text = "0";
            txtDiscount.Text = "0";
            txtTaxRate.Text = "0";

            txtRemarks.Text = $"PURCHASE ORDER - {GetOrderNo()}";

            txtOrderNo.ReadOnly = true;
            ViewState["IsNewRecord"] = "true";
            
            ShowModal();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["IsNewRecord"].ToString() == "true")
                    AddRecord();
                else
                    UpdateRecord();

            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

            LoadData();
            CloseModal();
        }

        protected void AddRecord()
        {            
            try
            {
                string query = $@"INSERT INTO tbl_purchase_order 
                                VALUES('{txtOrderNo.Text}',
                                    '{txtOrderDate.Text}',
                                    '{ddlVendorCompanyName.SelectedItem.Value}',
                                    '{ddlWarehouseCompanyName.SelectedItem.Value}',
                                    '{txtSubTotal.Text}',
                                    '{txtDiscount.Text}',
                                    '{txtTaxRate.Text}',
                                    '{txtShippingCost.Text}',
                                    '{txtTotal.Text}',
                                    '{txtRemarks.Text}',
                                    '{txtEntityName.Text}',
                                    '{txtEntityAddress.Text}',
                                    '{txtEntityPhone.Text}')";
                DbPurchaseOrder.runCommand(query);

                dt = (DataTable)ViewState["ShippingRecord"];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        query = $@"INSERT INTO tbl_shipping_details VALUES (
                            '{dr["Order No"].ToString()}',
                            '{dr["Shipping Name"].ToString()}',
                            '{dr["Shipping Method"].ToString()}',
                            '{dr["Shipping Terms"].ToString()}',
                            '{dr["Delivery Date"].ToString()}')";
                        DbPurchaseOrder.runCommand(query);
                    }
                }

                dt2 = (DataTable)ViewState["ProductRecord"];
                if (dt2.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt2.Rows)
                    {
                        query = $@"INSERT INTO tbl_product_details VALUES (
                            '{dr["Product Code"].ToString()}',
                            '{dr["Order No"].ToString()}',
                            '{dr["Quantity"].ToString()}',
                            '{dr["Delivery Date"].ToString()}')";
                        DbPurchaseOrder.runCommand(query);
                    }
                }
                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Insert purchase order data success');", true);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        protected void UpdateRecord()
        {
            try
            {
                if (string.IsNullOrEmpty(txtOrderNo.Text))
                    throw new ArgumentException();

                string query = $@"UPDATE tbl_purchase_order SET 
                                order_date = '{txtOrderDate.Text}',
                                supplier_id = '{ddlVendorCompanyName.SelectedItem.Value}',
                                warehouse_id = '{ddlWarehouseCompanyName.SelectedItem.Value}',
                                header_entity_name = '{txtEntityName.Text}',
                                header_entity_address = '{txtEntityAddress.Text}',
                                header_entity_phone = '{txtEntityPhone.Text}'
                                WHERE order_number = '{txtOrderNo.Text}'";
                DbPurchaseOrder.runCommand(query);

                dt = (DataTable)ViewState["ShippingRecord"];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        query = $"SELECT TOP 1 * FROM tbl_shipping_details WHERE id = '{dr["ID"].ToString()}'";
                        DataRow dr2 = DbPurchaseOrder.getRow(query);
                        if (dr2 == null)
                        {
                            query = $@"INSERT INTO tbl_shipping_details VALUES (
                                    '{dr["Order No"].ToString()}',
                                    '{dr["Shipping Name"].ToString()}',
                                    '{dr["Shipping Method"].ToString()}',
                                    '{dr["Shipping Terms"].ToString()}',
                                    '{dr["Delivery Date"].ToString()}')";
                            DbPurchaseOrder.runCommand(query);
                        }
                        else
                        {
                            query = $@"UPDATE tbl_shipping_details SET
                                    shipping_name = '{dr["Shipping Name"].ToString()}',
                                    shipping_method = '{dr["Shipping Method"].ToString()}',
                                    shipping_terms = '{dr["Shipping Terms"].ToString()}',
                                    delivery_date = '{dr["Delivery Date"].ToString()}'
                                    WHERE id = '{dr["ID"].ToString()}'";
                            DbPurchaseOrder.runCommand(query);
                        }                        
                    }
                }

                dt2 = (DataTable)ViewState["ProductRecord"];
                if (dt2.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt2.Rows)
                    {
                        query = $"SELECT TOP 1 * FROM tbl_product_details WHERE id = '{dr["ID"].ToString()}'";
                        DataRow dr2 = DbPurchaseOrder.getRow(query);
                        if (dr2 == null)
                        {
                            query = $@"INSERT INTO tbl_product_details VALUES (
                                    '{dr["Product Code"].ToString()}',
                                    '{dr["Order No"].ToString()}',
                                    '{dr["Quantity"].ToString()}',
                                    '{dr["Delivery Date"].ToString()}')";
                            DbPurchaseOrder.runCommand(query);
                        }
                        else
                        {
                            query = $@"UPDATE tbl_product_details SET
                                    product_code = '{dr["Product Code"].ToString()}',
                                    quantity = '{dr["Quantity"].ToString()}',
                                    delivery_date = '{dr["Delivery Date"].ToString()}'
                                    WHERE id = '{dr["ID"].ToString()}'";
                            DbPurchaseOrder.runCommand(query);
                        }
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Update purchase order data success');", true);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        protected void ShowModal()
        {
            ScriptManager.RegisterStartupScript(
                (Page)this, base.GetType(),
                "popup",
                "$('#btnPopup').click();",
                true
                );
        }

        protected void CloseModal()
        {
            ScriptManager.RegisterStartupScript(
                (Page)this, base.GetType(),
                "close",
                "$('#btnClose').click();",
                true
                );
        }
        protected string GetOrderNo()
        {
            string retval = string.Empty;
            try
            {
                int orderNo = 1;
                string query = @"SELECT IDENT_CURRENT('tbl_purchase_order') AS order_number";
                orderNo += Convert.ToInt32(DbPurchaseOrder.getFieldValue(query));

                retval = $"{orderNo.ToString().PadLeft(3, '0')}/KF/{DateTime.Now.ToString("MM")}/{DateTime.Now.ToString("yyyy")}";
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return retval;
        }
        #endregion

        #region Vendor
        protected void ddlVendorCompanyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabName.Value = "clickTabVendor";

            Models.Supplier vendor = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Supplier>(GetVendorData(ddlVendorCompanyName.SelectedItem.Value));

            txtVendorContactName.Text = vendor.supplier_contact_name;
            txtVendorAddress.Text = vendor.supplier_address;
            txtVendorPhone.Text = vendor.supplier_phone;
        }

        protected string GetVendorData(string id)
        {
            string retval = string.Empty;
            try
            {
                string query = $"SELECT TOP 1 * FROM tbl_supplier WHERE id = '{id}'";
                DataRow dr = DbPurchaseOrder.getRow(query);
                Models.Supplier vendor = new Models.Supplier();
                vendor.supplier_contact_name = dr["name"].ToString();
                vendor.supplier_address = dr["address"].ToString();
                vendor.supplier_phone = dr["phone"].ToString();

                retval = Newtonsoft.Json.JsonConvert.SerializeObject(vendor);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return retval;
        }

        protected void ClearFormVendor()
        {
            ddlVendorCompanyName.SelectedItem.Text = string.Empty;
            txtVendorContactName.Text = string.Empty;
            txtVendorAddress.Text = string.Empty;
            txtVendorPhone.Text = string.Empty;
        }
        #endregion

        #region Warehouse
        protected void ddlWarehouseCompanyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabName.Value = "clickTabCompany";

            Models.Warehouse warehouse = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Warehouse>(GetWarehouseData(ddlWarehouseCompanyName.SelectedItem.Value));

            txtWarehouseContactName.Text = warehouse.warehouse_contact_name;
            txtWarehouseDepartmentName.Text = warehouse.warehouse_department_name;
            txtWarehouseAddress.Text = warehouse.warehouse_address;
            txtWarehousePhone.Text = warehouse.warehouse_phone;
        }

        protected string GetWarehouseData(string id)
        {
            string retval = string.Empty;
            try
            {
                string query = $"SELECT TOP 1 * FROM tbl_warehouse WHERE id = '{id}'";
                DataRow dr = DbPurchaseOrder.getRow(query);
                Models.Warehouse warehouse = new Models.Warehouse();
                warehouse.warehouse_contact_name = dr["name"].ToString();
                warehouse.warehouse_department_name = dr["title"].ToString();
                warehouse.warehouse_address = dr["address"].ToString();
                warehouse.warehouse_phone = dr["phone"].ToString();

                retval = Newtonsoft.Json.JsonConvert.SerializeObject(warehouse);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return retval;
        }

        protected void ClearFormWarehouse()
        {
            ddlWarehouseCompanyName.SelectedItem.Text = string.Empty;
            txtWarehouseContactName.Text = string.Empty;
            txtWarehouseDepartmentName.Text = string.Empty;
            txtWarehouseAddress.Text = string.Empty;
            txtWarehousePhone.Text = string.Empty;
        }
        #endregion

        #region Shipping
        protected void btnShippingSave_Click(object sender, EventArgs e)
        {
            try
            {
                dt = (DataTable)ViewState["ShippingRecord"];
                dt.Rows.Add(GetShippingId(),txtOrderNo.Text, txtShippingName.Text, txtShippingMethod.Text, txtShippingTerms.Text, txtDeliveryDate.Text);
                GV_Shipping.DataSource = dt;
                GV_Shipping.DataBind();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

            ClearFormShipping();
            TabName.Value = "clickTabShipping";
        }

        protected void ClearFormShipping()
        {
            txtShippingName.Text = string.Empty;
            txtShippingMethod.Text = string.Empty;
            txtShippingTerms.Text = string.Empty;
            txtDeliveryDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        protected void LoadShippingData()
        {
            dt = (DataTable)ViewState["ShippingRecord"];
            GV_Shipping.DataSource = dt;
            GV_Shipping.DataBind();
        }

        protected void LoadPDFShippingData()
        {
            dt = (DataTable)ViewState["ShippingRecord"];
            GV_PDF_SHIPPING.DataSource = dt;
            GV_PDF_SHIPPING.DataBind();
        }

        protected string GetShippingId()
        {
            string retval = string.Empty;
            int ID = 1;
            try
            {
                dt = (DataTable)ViewState["ShippingRecord"];
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[dt.Rows.Count-1];
                    ID += Convert.ToInt32(dr["ID"].ToString());
                }
                else
                {
                    string query = @"SELECT IDENT_CURRENT('tbl_shipping_details') AS ID";
                    ID += Convert.ToInt32(DbPurchaseOrder.getFieldValue(query));
                }

                retval = ID.ToString();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return retval;
        }

        protected void GV_Shipping_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //link button delete
                    foreach (LinkButton button in e.Row.Cells[0].Controls.OfType<LinkButton>())
                    {
                        if (button.CommandName == "DeleteRow")
                        {
                            string key = e.Row.Cells[1].Text;

                            button.Attributes["onclick"] = "if(!confirm('Do you want to delete ID " + key + "?')){ return false; };";
                        }
                    }
                }
            }
            catch { }
        }

        protected void GV_Shipping_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteRow")
                {
                    //get id
                    string key = e.CommandArgument.ToString();

                    //delete data
                    DeleteShippingRecord(key);
                }
            }
            catch { }
        }

        protected void DeleteShippingRecord(string id)
        {
            TabName.Value = "clickTabShipping";
            try
            {
                dt = (DataTable)ViewState["ShippingRecord"];
                foreach (DataRow dr in dt.Rows)
                {
                    if (id == dr["ID"].ToString())
                    {
                        dt.Rows.Remove(dr);
                        break;
                    }                        
                }
                ViewState["ShippingRecord"] = dt;
                LoadShippingData();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        #endregion

        #region Product
        protected void ddlProductCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            TabName.Value = "clickTabProduct";

            Models.Product product = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.Product>(GetProductData(ddlProductCode.SelectedItem.Value));

            txtProductName.Text = product.product_name;
            txtUnitPrice.Text = product.product_price;
        }
        protected string GetProductData(string code)
        {
            string retval = string.Empty;
            try
            {
                string query = $"SELECT TOP 1 * FROM tbl_product WHERE product_code = '{code}'";
                DataRow dr = DbPurchaseOrder.getRow(query);
                Models.Product product = new Models.Product();
                product.product_code = dr["product_code"].ToString();
                product.product_name = dr["product_name"].ToString();
                product.product_price = dr["product_price"].ToString();

                retval = Newtonsoft.Json.JsonConvert.SerializeObject(product);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return retval;
        }
        protected void CalculateSubTotal()
        {
            int sub_total = 0;
            decimal shipping_cost = 0.01M;
            dt2 = (DataTable)ViewState["ProductRecord"];

            foreach (DataRow dr in dt2.Rows)
            {
                sub_total += (Convert.ToInt32(dr["Quantity"].ToString()) * Convert.ToInt32(dr["Unit Price"].ToString()));
            }
            shipping_cost *= sub_total;
            txtSubTotal.Text = sub_total.ToString();
            txtShippingCost.Text = Convert.ToInt32(shipping_cost).ToString();
        }
        protected void ClearFormProduct()
        {
            ddlProductCode.SelectedIndex = -1;
            txtProductName.Text = string.Empty;
            txtQuantity.Text = "0";
            txtUnitPrice.Text = string.Empty;
            txtProductDeliveryDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }
        protected void LoadProductData()
        {
            dt2 = (DataTable)ViewState["ProductRecord"];
            GV_Product.DataSource = dt2;
            GV_Product.DataBind();
        }
        protected void LoadPDFProductData()
        {
            dt2 = (DataTable)ViewState["ProductRecord"];
            GV_PDF_PRODUCT.DataSource = dt2;
            GV_PDF_PRODUCT.DataBind();
        }
        protected void btnProductSave_Click(object sender, EventArgs e)
        {
            try
            {
                dt2 = (DataTable)ViewState["ProductRecord"];
                dt2.Rows.Add(GetProductId(), txtOrderNo.Text, ddlProductCode.SelectedItem.Text, txtProductName.Text, txtQuantity.Text, txtUnitPrice.Text, txtDeliveryDate.Text);
                GV_Product.DataSource = dt2;
                GV_Product.DataBind();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

            ClearFormProduct();
            CalculateSubTotal();
            TabName.Value = "clickTabProduct";
        }

        protected string GetProductId()
        {
            string retval = string.Empty;
            int ID = 1;
            try
            {
                dt2 = (DataTable)ViewState["ProductRecord"];
                if (dt2.Rows.Count > 0)
                {
                    DataRow dr = dt2.Rows[dt2.Rows.Count - 1];
                    ID += Convert.ToInt32(dr["ID"].ToString());
                }
                else
                {
                    string query = @"SELECT IDENT_CURRENT('tbl_product_details') AS ID";
                    ID += Convert.ToInt32(DbPurchaseOrder.getFieldValue(query));
                }

                retval = ID.ToString();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            return retval;
        }

        protected void GV_Product_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //link button delete
                    foreach (LinkButton button in e.Row.Cells[0].Controls.OfType<LinkButton>())
                    {
                        if (button.CommandName == "DeleteRow")
                        {
                            string key = e.Row.Cells[1].Text;

                            button.Attributes["onclick"] = "if(!confirm('Do you want to delete ID " + key + "?')){ return false; };";
                        }
                    }
                }
            }
            catch { }
        }

        protected void GV_Product_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "DeleteRow")
                {
                    //get id
                    string key = e.CommandArgument.ToString();

                    //delete data
                    DeleteProductRecord(key);
                }
            }
            catch { }
        }

        protected void DeleteProductRecord(string id)
        {
            TabName.Value = "clickTabProduct";
            try
            {
                dt2 = (DataTable)ViewState["ProductRecord"];
                foreach (DataRow dr in dt2.Rows)
                {
                    if (id == dr["ID"].ToString())
                    {
                        dt2.Rows.Remove(dr);
                        break;
                    }
                }
                ViewState["ProductRecord"] = dt2;
                LoadProductData();
                CalculateSubTotal();
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        protected void txtDiscount_TextChanged(object sender, EventArgs e)
        {
            int sub_total_discount = 0;
            string sub_total = txtSubTotal.Text;

            sub_total_discount += Convert.ToInt32(Convert.ToDouble(sub_total) * ((100 - Convert.ToDouble(txtDiscount.Text))/100));
            txtSubTotalDiscount.Text = sub_total_discount.ToString();
        }
        #endregion

        protected void txtTaxRate_TextChanged(object sender, EventArgs e)
        {
            int total_tax = 0;
            string sub_total = txtSubTotal.Text;

            total_tax += Convert.ToInt32(Convert.ToDouble(sub_total) * (Convert.ToDouble(txtDiscount.Text) / 100));
            txtTotalTax.Text = total_tax.ToString();

            int total = 0;
            int sub_total_discount = Convert.ToInt32(txtSubTotalDiscount.Text);
            int shipping_cost = Convert.ToInt32(txtShippingCost.Text);
            total += sub_total_discount + total_tax + shipping_cost;
            txtTotal.Text = total.ToString();
        }
    }
}