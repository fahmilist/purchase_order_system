﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Supplier.aspx.cs" Inherits="PurchaseOrderApp.Views.Supplier" MasterPageFile="~/Site.Master"%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" style="text-align: left;">
                    <li class="active"><a href="#first" data-toggle="tab" id="first_tab">List Supplier</a></li>
                    <%--<li><a href="#second" data-toggle="tab" id="second_tab">Add Supplier</a></li>--%>
                </ul>
    
                <div class="tab-content">
                    <div id="first" class="tab-pane fade in active">
                        <asp:GridView ID="GV_Supplier" runat="server"
                            AllowPaging="true"
                            AllowSorting="false"
                            OnRowDataBound="GV_Supplier_RowDataBound"
                            OnRowCommand="GV_Supplier_RowCommand"
                            OnPageIndexChanging="GV_Supplier_PageIndexChanging"
                            PagerStyle-CssClass="arn-pagination"
                            CssClass="table box table-hover table-striped table-bordered">
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="5px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" runat="server"
                                            CommandArgument='<%# Eval("id") %>'
                                            CommandName="EditRow"
                                            CssClass="fa fa-edit"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="5px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDelete" runat="server"
                                            CommandArgument='<%# Eval("id") %>'
                                            CommandName="DeleteRow"
                                            CssClass="fa fa-remove"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>                        
                        <asp:LinkButton ID="btnAdd" CssClass="btn btn-primary btn-sm" runat="server" OnClick="btnAdd_Click">ADD</asp:LinkButton>
                        <%-- hide button edit supplier--%>
                        <a class="btn btn-primary btn-md" onclick="$('#second_tab').trigger('click')" id="btnMove" style="display: none">Edit</a>
                        <%-- hide button add supplier--%>
                        <button type="button" id="btnPopup" data-toggle="modal" data-target="#myModal"
                            class="btn btn-primary btn-sm" style="display: none">
                        </button>
                    </div>
      
                    <div id="second" class="tab-pane fade in">
                        <div class="box-body">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label class="control-label">Requester</label>
                                    </div>                                
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="TxtRequester" runat="server" placeholder="Requester" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                    </div>                       
                                </div>                   
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label class="control-label">Order Date</label>
                                    </div>                                
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="TxtOrderDate" runat="server" placeholder="yyyy-mm-dd" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <label class="control-label">Vendor name</label>
                                    </div>                                
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="TxtSupplier" runat="server" placeholder="Requester" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                    
                        </div>
                        <a class="btn btn-primary btn-md" onclick="$('#first_tab').trigger('click')">Add</a>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    
    <div class="modal" id="myModal">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Input Supplier</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="txtPICName" class="col-sm-3 control-label">PIC name</label>

                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtPICName" runat="server" CssClass="form-control" MaxLength="50" required="true"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="txtEntityName" class="col-sm-3 control-label">Entity name</label>

                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtEntityName" runat="server" CssClass="form-control" MaxLength="100" required="true"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="txtAddress" class="col-sm-3 control-label">Address</label>

                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" MaxLength="100" required="true"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="txtPhoneNumber" class="col-sm-3 control-label">Phone number</label>

                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="form-control" MaxLength="15" required="true" TextMode="Number" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="btnClose" type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSave_Click" />
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script>
        $(function () {
            //Date picker
            $(<%=TxtOrderDate.ClientID%>).datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });

        })
    </script>
</asp:Content>
