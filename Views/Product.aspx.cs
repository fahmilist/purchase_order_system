﻿using PurchaseOrderApp.App_Start;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PurchaseOrderApp.Views
{
    public partial class Product : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                InitDefaultValue();
                LoadData();
            }
        }

        private void InitDefaultValue()
        {
            TxtRequester.Text = "PT. Kalbe Farma Tbk.";

            //set default time
            if (string.IsNullOrEmpty(TxtOrderDate.Text))
                TxtOrderDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
        }

        private void LoadData()
        {
            DbPurchaseOrder.LoadParameter();
            string query = @"SELECT * FROM tbl_product";
            DataTable dt = DbPurchaseOrder.getRecords(query);
            GV_Product.DataSource = dt;
            GV_Product.DataBind();
        }

        #region table product
        protected void GV_Product_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "EditRow" || e.CommandName == "DeleteRow")
                {
                    //get id
                    string key = e.CommandArgument.ToString();

                    if (e.CommandName == "EditRow")
                    {
                        //display data
                        ViewRecord(key);
                    }
                    else if (e.CommandName == "DeleteRow")
                    {
                        //delete data
                        DeleteRecord(key);
                    }
                }
            }
            catch { }
        }

        protected void GV_Product_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //link button delete
                    foreach (LinkButton button in e.Row.Cells[1].Controls.OfType<LinkButton>())
                    {
                        if (button.CommandName == "DeleteRow")
                        {
                            string key = e.Row.Cells[2].Text;

                            button.Attributes["onclick"] = "if(!confirm('Do you want to delete id " + key + "?')){ return false; };";
                        }
                    }
                }
            }
            catch { }
        }

        protected void GV_Product_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //bind gridview
                GV_Product.PageIndex = e.NewPageIndex;
                LoadData();
            }
            catch { }
        }
        protected void ViewRecord(string id)
        {
            try
            {
                string query = $"SELECT * FROM tbl_product WHERE product_code = '{id}'";
                DataRow dr = DbPurchaseOrder.getRow(query);
                TxtProduct.Text = dr["product_name"].ToString();

                txtProductCode.Text = dr["product_code"].ToString();
                txtProductName.Text = dr["product_name"].ToString();
                txtProductPrice.Text = dr["product_price"].ToString();

                //disable edit product code
                txtProductCode.ReadOnly = true;

                ViewState["IsNewRecord"] = "false";
                ShowModal();
                //ScriptManager.RegisterStartupScript(this, GetType(), "script", "$('#btnMove').click();", true);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        protected void DeleteRecord(string id)
        {
            try
            {
                string query = $"DELETE FROM tbl_product WHERE product_code = '{id}'";
                DbPurchaseOrder.runCommand(query);

                LoadData();
            }
            catch { }
        }
        #endregion

        #region Add / Update record
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            txtProductCode.Text = string.Empty;
            txtProductName.Text = string.Empty;
            txtProductPrice.Text = string.Empty;

            txtProductCode.ReadOnly = false;
            ViewState["IsNewRecord"] = "true";
            ShowModal();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["IsNewRecord"].ToString() == "true")
                    AddRecord();
                else
                    UpdateRecord();

            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

            LoadData();
            CloseModal();
        }

        protected void AddRecord()
        {
            try
            {
                string query = $@"INSERT INTO tbl_product 
                                VALUES('{txtProductCode.Text}',
                                    '{txtProductName.Text}',
                                    '{txtProductPrice.Text}')";
                DbPurchaseOrder.runCommand(query);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Insert product data success');", true);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        protected void UpdateRecord()
        {
            try
            {
                if (string.IsNullOrEmpty(txtProductCode.Text))
                    throw new ArgumentException();

                string query = $@"UPDATE tbl_product SET 
                                product_name = '{txtProductName.Text}',
                                product_price = '{txtProductPrice.Text}'
                                WHERE product_code = '{txtProductCode.Text}'";
                DbPurchaseOrder.runCommand(query);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "alert('Update product data success');", true);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }
        protected void ShowModal()
        {
            ScriptManager.RegisterStartupScript(
                (Page)this, base.GetType(),
                "popup",
                "$('#btnPopup').click();",
                true
                );
        }

        protected void CloseModal()
        {
            ScriptManager.RegisterStartupScript(
                (Page)this, base.GetType(),
                "close",
                "$('#btnClose').click();",
                true
                );
        }

        #endregion        
    }
}