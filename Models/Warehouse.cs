﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurchaseOrderApp.Models
{
    public class Warehouse
    {
        public string warehouse_contact_name { get; set; }
        public string warehouse_department_name { get; set; }
        public string warehouse_address { get; set; }
        public string warehouse_phone { get; set; }
    }
}