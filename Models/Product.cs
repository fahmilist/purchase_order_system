﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurchaseOrderApp.Models
{
    public class Product
    {
        public string product_code { get; set; }
        public string product_name { get; set; }
        public string product_price { get; set; }
    }
}