﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurchaseOrderApp.Models
{
    public class Supplier
    {
        public string supplier_contact_name { get; set; }
        public string supplier_address { get; set; }
        public string supplier_phone { get; set; }
    }
}