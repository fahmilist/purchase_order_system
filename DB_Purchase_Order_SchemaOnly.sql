USE [db_purchase_order]
GO
/****** Object:  Table [dbo].[tbl_product]    Script Date: 3/29/2021 8:58:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_product](
	[product_code] [varchar](50) NOT NULL,
	[product_name] [varchar](100) NULL,
	[product_price] [float] NULL,
 CONSTRAINT [PK_tbl_product_1] PRIMARY KEY CLUSTERED 
(
	[product_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_product_details]    Script Date: 3/29/2021 8:58:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_product_details](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[product_code] [varchar](50) NULL,
	[order_number] [varchar](100) NULL,
	[quantity] [int] NULL,
	[delivery_date] [date] NULL,
 CONSTRAINT [PK_tbl_product_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_purchase_order]    Script Date: 3/29/2021 8:58:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchase_order](
	[order_id] [bigint] IDENTITY(1,1) NOT NULL,
	[order_number] [varchar](100) NOT NULL,
	[order_date] [date] NULL,
	[supplier_id] [int] NULL,
	[warehouse_id] [int] NULL,
	[subtotal] [varchar](100) NULL,
	[discount] [float] NULL,
	[tax_rate] [float] NULL,
	[shipping_cost] [float] NULL,
	[total] [varchar](100) NULL,
	[notes] [varchar](100) NULL,
	[header_entity_name] [varchar](100) NULL,
	[header_entity_address] [varchar](100) NULL,
	[header_entity_phone] [varchar](16) NULL,
 CONSTRAINT [PK_tbl_purchase_order] PRIMARY KEY CLUSTERED 
(
	[order_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_shipping_details]    Script Date: 3/29/2021 8:58:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_shipping_details](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[order_number] [varchar](100) NULL,
	[shipping_name] [varchar](50) NULL,
	[shipping_method] [varchar](50) NULL,
	[shipping_terms] [varchar](50) NULL,
	[delivery_date] [date] NULL,
 CONSTRAINT [PK_tbl_shipping_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_supplier]    Script Date: 3/29/2021 8:58:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_supplier](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[entity] [varchar](100) NULL,
	[address] [varchar](100) NULL,
	[phone] [varchar](20) NULL,
 CONSTRAINT [PK_tbl_supplier] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_warehouse]    Script Date: 3/29/2021 8:58:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_warehouse](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[title] [varchar](50) NULL,
	[entity] [varchar](100) NULL,
	[address] [varchar](100) NULL,
	[phone] [varchar](20) NULL,
 CONSTRAINT [PK_tbl_warehouse] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_tbl_purchase_order]    Script Date: 3/29/2021 8:58:15 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_tbl_purchase_order] ON [dbo].[tbl_purchase_order]
(
	[order_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_product_details]  WITH CHECK ADD  CONSTRAINT [FK_tbl_product_details_tbl_product] FOREIGN KEY([product_code])
REFERENCES [dbo].[tbl_product] ([product_code])
GO
ALTER TABLE [dbo].[tbl_product_details] CHECK CONSTRAINT [FK_tbl_product_details_tbl_product]
GO
ALTER TABLE [dbo].[tbl_purchase_order]  WITH CHECK ADD  CONSTRAINT [FK_Supplier_PurchaseOrder] FOREIGN KEY([supplier_id])
REFERENCES [dbo].[tbl_supplier] ([id])
GO
ALTER TABLE [dbo].[tbl_purchase_order] CHECK CONSTRAINT [FK_Supplier_PurchaseOrder]
GO
ALTER TABLE [dbo].[tbl_purchase_order]  WITH CHECK ADD  CONSTRAINT [FK_Warehouse_PurchaseOrder] FOREIGN KEY([warehouse_id])
REFERENCES [dbo].[tbl_warehouse] ([id])
GO
ALTER TABLE [dbo].[tbl_purchase_order] CHECK CONSTRAINT [FK_Warehouse_PurchaseOrder]
GO
