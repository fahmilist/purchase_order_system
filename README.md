# purchase_order_system

Mini Project - Kalbe Farma

1) Restore DB with data or schema only:
- Restore DB_Purchase_Order.sql (Data & Schema)
- Restore DB_Purchase_Order_SchemaOnly.sql (Schema)

2) Setting DB environment:
- Open CONFIG.xml
- Fill with your SQL Server Credentials
