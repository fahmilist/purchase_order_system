USE [master]
GO
/****** Object:  Database [db_purchase_order]    Script Date: 3/29/2021 8:55:32 AM ******/
CREATE DATABASE [db_purchase_order]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_purchase_order', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\db_purchase_order.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'db_purchase_order_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\db_purchase_order_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [db_purchase_order] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_purchase_order].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_purchase_order] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_purchase_order] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_purchase_order] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_purchase_order] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_purchase_order] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_purchase_order] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_purchase_order] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_purchase_order] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_purchase_order] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_purchase_order] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_purchase_order] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_purchase_order] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_purchase_order] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_purchase_order] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_purchase_order] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_purchase_order] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_purchase_order] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_purchase_order] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_purchase_order] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_purchase_order] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_purchase_order] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_purchase_order] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_purchase_order] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [db_purchase_order] SET  MULTI_USER 
GO
ALTER DATABASE [db_purchase_order] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_purchase_order] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_purchase_order] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_purchase_order] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [db_purchase_order] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [db_purchase_order] SET QUERY_STORE = OFF
GO
USE [db_purchase_order]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [db_purchase_order]
GO
/****** Object:  Table [dbo].[tbl_product]    Script Date: 3/29/2021 8:55:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_product](
	[product_code] [varchar](50) NOT NULL,
	[product_name] [varchar](100) NULL,
	[product_price] [float] NULL,
 CONSTRAINT [PK_tbl_product_1] PRIMARY KEY CLUSTERED 
(
	[product_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_product_details]    Script Date: 3/29/2021 8:55:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_product_details](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[product_code] [varchar](50) NULL,
	[order_number] [varchar](100) NULL,
	[quantity] [int] NULL,
	[delivery_date] [date] NULL,
 CONSTRAINT [PK_tbl_product_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_purchase_order]    Script Date: 3/29/2021 8:55:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_purchase_order](
	[order_id] [bigint] IDENTITY(1,1) NOT NULL,
	[order_number] [varchar](100) NOT NULL,
	[order_date] [date] NULL,
	[supplier_id] [int] NULL,
	[warehouse_id] [int] NULL,
	[subtotal] [varchar](100) NULL,
	[discount] [float] NULL,
	[tax_rate] [float] NULL,
	[shipping_cost] [float] NULL,
	[total] [varchar](100) NULL,
	[notes] [varchar](100) NULL,
	[header_entity_name] [varchar](100) NULL,
	[header_entity_address] [varchar](100) NULL,
	[header_entity_phone] [varchar](16) NULL,
 CONSTRAINT [PK_tbl_purchase_order] PRIMARY KEY CLUSTERED 
(
	[order_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_shipping_details]    Script Date: 3/29/2021 8:55:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_shipping_details](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[order_number] [varchar](100) NULL,
	[shipping_name] [varchar](50) NULL,
	[shipping_method] [varchar](50) NULL,
	[shipping_terms] [varchar](50) NULL,
	[delivery_date] [date] NULL,
 CONSTRAINT [PK_tbl_shipping_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_supplier]    Script Date: 3/29/2021 8:55:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_supplier](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[entity] [varchar](100) NULL,
	[address] [varchar](100) NULL,
	[phone] [varchar](20) NULL,
 CONSTRAINT [PK_tbl_supplier] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_warehouse]    Script Date: 3/29/2021 8:55:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_warehouse](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[title] [varchar](50) NULL,
	[entity] [varchar](100) NULL,
	[address] [varchar](100) NULL,
	[phone] [varchar](20) NULL,
 CONSTRAINT [PK_tbl_warehouse] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[tbl_product] ([product_code], [product_name], [product_price]) VALUES (N'101', N'Cilok', 5000)
GO
INSERT [dbo].[tbl_product] ([product_code], [product_name], [product_price]) VALUES (N'202', N'Charger', 10000)
GO
INSERT [dbo].[tbl_product] ([product_code], [product_name], [product_price]) VALUES (N'AAA-123', N'Asus ROG 123', 12999999.9933332)
GO
INSERT [dbo].[tbl_product] ([product_code], [product_name], [product_price]) VALUES (N'BBB-456', N'Batik Keris 456', 399999)
GO
INSERT [dbo].[tbl_product] ([product_code], [product_name], [product_price]) VALUES (N'CCC-789', N'Corsair 789', 799500.599)
GO
INSERT [dbo].[tbl_product] ([product_code], [product_name], [product_price]) VALUES (N'DDD-001', N'Donat 001', 900.99)
GO
INSERT [dbo].[tbl_product] ([product_code], [product_name], [product_price]) VALUES (N'EEE-100', N'Eclair 100', 5555.9907)
GO
SET IDENTITY_INSERT [dbo].[tbl_product_details] ON 
GO
INSERT [dbo].[tbl_product_details] ([id], [product_code], [order_number], [quantity], [delivery_date]) VALUES (1, N'101', N'1', 10, CAST(N'2021-03-28' AS Date))
GO
INSERT [dbo].[tbl_product_details] ([id], [product_code], [order_number], [quantity], [delivery_date]) VALUES (2, N'202', N'1', 5, CAST(N'2021-03-28' AS Date))
GO
INSERT [dbo].[tbl_product_details] ([id], [product_code], [order_number], [quantity], [delivery_date]) VALUES (3, N'101', N'2', 2, CAST(N'2021-03-28' AS Date))
GO
INSERT [dbo].[tbl_product_details] ([id], [product_code], [order_number], [quantity], [delivery_date]) VALUES (4, N'202', N'2', 1, CAST(N'2021-03-28' AS Date))
GO
INSERT [dbo].[tbl_product_details] ([id], [product_code], [order_number], [quantity], [delivery_date]) VALUES (5, N'101', N'3', 6, CAST(N'2021-03-28' AS Date))
GO
INSERT [dbo].[tbl_product_details] ([id], [product_code], [order_number], [quantity], [delivery_date]) VALUES (6, N'202', N'3', 2, CAST(N'2021-03-28' AS Date))
GO
INSERT [dbo].[tbl_product_details] ([id], [product_code], [order_number], [quantity], [delivery_date]) VALUES (7, N'202', N'009/KF/03/2021', 10, CAST(N'2021-03-29' AS Date))
GO
INSERT [dbo].[tbl_product_details] ([id], [product_code], [order_number], [quantity], [delivery_date]) VALUES (8, N'101', N'009/KF/03/2021', 10, CAST(N'2021-03-29' AS Date))
GO
INSERT [dbo].[tbl_product_details] ([id], [product_code], [order_number], [quantity], [delivery_date]) VALUES (9, N'101', N'010/KF/03/2021', 6, CAST(N'2021-03-29' AS Date))
GO
INSERT [dbo].[tbl_product_details] ([id], [product_code], [order_number], [quantity], [delivery_date]) VALUES (10, N'202', N'010/KF/03/2021', 2, CAST(N'2021-03-29' AS Date))
GO
SET IDENTITY_INSERT [dbo].[tbl_product_details] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_purchase_order] ON 
GO
INSERT [dbo].[tbl_purchase_order] ([order_id], [order_number], [order_date], [supplier_id], [warehouse_id], [subtotal], [discount], [tax_rate], [shipping_cost], [total], [notes], [header_entity_name], [header_entity_address], [header_entity_phone]) VALUES (1, N'001/KF/03/2021', CAST(N'2021-03-28' AS Date), 1, 1, N'100000', 0, 0, 19000, N'119000', N'Jangan pake saos', N'Stella', NULL, NULL)
GO
INSERT [dbo].[tbl_purchase_order] ([order_id], [order_number], [order_date], [supplier_id], [warehouse_id], [subtotal], [discount], [tax_rate], [shipping_cost], [total], [notes], [header_entity_name], [header_entity_address], [header_entity_phone]) VALUES (2, N'002/KF/03/2021', CAST(N'2021-03-28' AS Date), 4, 4, N'20000', 0, 0, 3000, N'23000', N'Pedes', N'Porco', NULL, NULL)
GO
INSERT [dbo].[tbl_purchase_order] ([order_id], [order_number], [order_date], [supplier_id], [warehouse_id], [subtotal], [discount], [tax_rate], [shipping_cost], [total], [notes], [header_entity_name], [header_entity_address], [header_entity_phone]) VALUES (3, N'003/KF/03/2021', CAST(N'2021-03-28' AS Date), 3, 3, N'50000', 0, 0, 9000, N'59000', N'Jangan pake kecap', N'Magath', NULL, NULL)
GO
INSERT [dbo].[tbl_purchase_order] ([order_id], [order_number], [order_date], [supplier_id], [warehouse_id], [subtotal], [discount], [tax_rate], [shipping_cost], [total], [notes], [header_entity_name], [header_entity_address], [header_entity_phone]) VALUES (9, N'009/KF/03/2021', CAST(N'2021-03-29' AS Date), 7, 9, N'150000', 10, 5, 1500, N'151500', N'TEST', N'PT. Kalbe Farma Tbk.', N'Jl. Rawa Gatel, Pulogadung.', N'0214600158')
GO
INSERT [dbo].[tbl_purchase_order] ([order_id], [order_number], [order_date], [supplier_id], [warehouse_id], [subtotal], [discount], [tax_rate], [shipping_cost], [total], [notes], [header_entity_name], [header_entity_address], [header_entity_phone]) VALUES (10, N'010/KF/03/2021', CAST(N'2021-03-29' AS Date), 11, 5, N'50000', 0, 0, 500, N'50500', N'PURCHASE ORDER - 010/KF/03/2021', N'PT. Kalbe Farma Tbk.', N'Jl. Rawa Gatel, Pulogadung.', N'0214600158')
GO
SET IDENTITY_INSERT [dbo].[tbl_purchase_order] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_shipping_details] ON 
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (6, N'1', N'GoJek', N'SameDay', N'Wajib Asuransi', CAST(N'2021-03-28' AS Date))
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (7, N'1', N'Grab', N'Express', N'Wajib Asuransi', CAST(N'2021-03-28' AS Date))
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (8, N'2', N'JNT', N'Express', N'Wajib Asuransi', CAST(N'2021-03-28' AS Date))
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (9, N'3', N'SiCepat', N'Kilat', N'Wajib Asuransi', CAST(N'2021-03-28' AS Date))
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (10, N'3', N'JNE', N'YES', N'Wajib Asuransi', CAST(N'2021-03-28' AS Date))
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (11, N'3', N'TIKI', N'Express', N'Wajib Asuransi', CAST(N'2021-03-28' AS Date))
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (12, N'008/KF/03/2021', N'JNT', N'Express', N'6-8 jam', CAST(N'2021-03-29' AS Date))
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (13, N'008/KF/03/2021', N'Gojek', N'SameDay', N'6-8 jam', CAST(N'2021-03-29' AS Date))
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (14, N'008/KF/03/2021', N'JNT', N'Express', N'6-8 jam', CAST(N'2021-03-29' AS Date))
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (15, N'009/KF/03/2021', N'Gojek', N'SameDay', N'6-8 jam', CAST(N'2021-03-29' AS Date))
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (16, N'009/KF/03/2021', N'JNT', N'Express', N'6-8 jam', CAST(N'2021-03-29' AS Date))
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (17, N'010/KF/03/2021', N'Gojek', N'SameDay', N'6-8 jam', CAST(N'2021-03-29' AS Date))
GO
INSERT [dbo].[tbl_shipping_details] ([id], [order_number], [shipping_name], [shipping_method], [shipping_terms], [delivery_date]) VALUES (18, N'010/KF/03/2021', N'Grab', N'Express', N'6-8 jam', CAST(N'2021-03-29' AS Date))
GO
SET IDENTITY_INSERT [dbo].[tbl_shipping_details] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_supplier] ON 
GO
INSERT [dbo].[tbl_supplier] ([id], [name], [entity], [address], [phone]) VALUES (1, N'Ymir', N'PT. SNK Indonesia Tbk.', N'Jakarta Pusat', N'081288004175')
GO
INSERT [dbo].[tbl_supplier] ([id], [name], [entity], [address], [phone]) VALUES (3, N'Eren', N'PT. SNK Indonesia Tbk.', N'Jakarta Pusat', N'021998877661')
GO
INSERT [dbo].[tbl_supplier] ([id], [name], [entity], [address], [phone]) VALUES (4, N'Connie', N'PT. SNK Indonesia Tbk.', N'Jakarta Pusat', N'021898989')
GO
INSERT [dbo].[tbl_supplier] ([id], [name], [entity], [address], [phone]) VALUES (5, N'Mikasa', N'PT. SNK Indonesia Tbk.', N'Jakarta Pusat', N'0218989891')
GO
INSERT [dbo].[tbl_supplier] ([id], [name], [entity], [address], [phone]) VALUES (6, N'Levi', N'PT. SNK Indonesia Tbk.', N'Jakarta Pusat', N'0217788991')
GO
INSERT [dbo].[tbl_supplier] ([id], [name], [entity], [address], [phone]) VALUES (7, N'Armin', N'PT. SNK Indonesia Tbk.', N'Jakarta Pusat', N'0213333881818')
GO
INSERT [dbo].[tbl_supplier] ([id], [name], [entity], [address], [phone]) VALUES (8, N'Gabi', N'PT. SNK Indonesia Tbk.', N'Jakarta Pusat', N'0219990922')
GO
INSERT [dbo].[tbl_supplier] ([id], [name], [entity], [address], [phone]) VALUES (9, N'Falco', N'PT. SNK Indonesia Tbk.', N'Jakarta Pusat', N'02019993939')
GO
INSERT [dbo].[tbl_supplier] ([id], [name], [entity], [address], [phone]) VALUES (10, N'Reiner', N'PT. SNK Indonesia Tbk.', N'Jakarta Pusat', N'0213838388129')
GO
INSERT [dbo].[tbl_supplier] ([id], [name], [entity], [address], [phone]) VALUES (11, N'Jean', N'PT. SNK Indonesia Tbk.', N'Jakarta Pusat', N'021838381828')
GO
INSERT [dbo].[tbl_supplier] ([id], [name], [entity], [address], [phone]) VALUES (12, N'Historia', N'PT. SNK Indonesia Tbk.', N'Jakarta Pusat', N'021939391929')
GO
SET IDENTITY_INSERT [dbo].[tbl_supplier] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_warehouse] ON 
GO
INSERT [dbo].[tbl_warehouse] ([id], [name], [title], [entity], [address], [phone]) VALUES (1, N'Luffy', N'Captain', N'PT. ONE Indonesia Tbk.', N'Bandung', N'02193939129')
GO
INSERT [dbo].[tbl_warehouse] ([id], [name], [title], [entity], [address], [phone]) VALUES (2, N'Ace', N'Vice Captain', N'PT. ONE Indonesia Tbk.', N'Bandung', N'02193939392')
GO
INSERT [dbo].[tbl_warehouse] ([id], [name], [title], [entity], [address], [phone]) VALUES (3, N'Roger', N'King', N'PT. ONE Indonesia Tbk.', N'Bandung', N'0219383282938')
GO
INSERT [dbo].[tbl_warehouse] ([id], [name], [title], [entity], [address], [phone]) VALUES (4, N'Nami', N'Crew', N'PT. ONE Indonesia Tbk.', N'Bandung', N'021393920390')
GO
INSERT [dbo].[tbl_warehouse] ([id], [name], [title], [entity], [address], [phone]) VALUES (5, N'Usopp', N'Crew', N'PT. ONE Indonesia Tbk.', N'Bandung', N'02139392902')
GO
INSERT [dbo].[tbl_warehouse] ([id], [name], [title], [entity], [address], [phone]) VALUES (6, N'Zoro', N'Vice Captain', N'PT. ONE Indonesia Tbk.', N'Bandung', N'0219399329039')
GO
INSERT [dbo].[tbl_warehouse] ([id], [name], [title], [entity], [address], [phone]) VALUES (7, N'Shirohige', N'Captain', N'PT. ONE Indonesia Tbk.', N'Bandung', N'021939392392')
GO
INSERT [dbo].[tbl_warehouse] ([id], [name], [title], [entity], [address], [phone]) VALUES (8, N'Kurohige', N'Captain', N'PT. ONE Indonesia Tbk.', N'Bandung', N'021392903920')
GO
INSERT [dbo].[tbl_warehouse] ([id], [name], [title], [entity], [address], [phone]) VALUES (9, N'Big Mom', N'Yonko', N'PT. ONE Indonesia Tbk.', N'Bandung', N'0219320292')
GO
INSERT [dbo].[tbl_warehouse] ([id], [name], [title], [entity], [address], [phone]) VALUES (10, N'Kaido', N'Yonko', N'PT. ONE Indonesia Tbk.', N'Bandung', N'021393932929')
GO
INSERT [dbo].[tbl_warehouse] ([id], [name], [title], [entity], [address], [phone]) VALUES (11, N'Sengoku', N'Fleet Admiral', N'PT. ONE Indonesia Tbk.', N'Bandung', N'021932398292')
GO
INSERT [dbo].[tbl_warehouse] ([id], [name], [title], [entity], [address], [phone]) VALUES (12, N'Akainu', N'Admiral', N'PT. ONE Indonesia Tbk.', N'Bandung', N'0213939029092')
GO
SET IDENTITY_INSERT [dbo].[tbl_warehouse] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_tbl_purchase_order]    Script Date: 3/29/2021 8:55:34 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_tbl_purchase_order] ON [dbo].[tbl_purchase_order]
(
	[order_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_product_details]  WITH CHECK ADD  CONSTRAINT [FK_tbl_product_details_tbl_product] FOREIGN KEY([product_code])
REFERENCES [dbo].[tbl_product] ([product_code])
GO
ALTER TABLE [dbo].[tbl_product_details] CHECK CONSTRAINT [FK_tbl_product_details_tbl_product]
GO
ALTER TABLE [dbo].[tbl_purchase_order]  WITH CHECK ADD  CONSTRAINT [FK_Supplier_PurchaseOrder] FOREIGN KEY([supplier_id])
REFERENCES [dbo].[tbl_supplier] ([id])
GO
ALTER TABLE [dbo].[tbl_purchase_order] CHECK CONSTRAINT [FK_Supplier_PurchaseOrder]
GO
ALTER TABLE [dbo].[tbl_purchase_order]  WITH CHECK ADD  CONSTRAINT [FK_Warehouse_PurchaseOrder] FOREIGN KEY([warehouse_id])
REFERENCES [dbo].[tbl_warehouse] ([id])
GO
ALTER TABLE [dbo].[tbl_purchase_order] CHECK CONSTRAINT [FK_Warehouse_PurchaseOrder]
GO
USE [master]
GO
ALTER DATABASE [db_purchase_order] SET  READ_WRITE 
GO
